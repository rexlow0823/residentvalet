import firebase from 'firebase';

import {
  REGISTER_VISITOR_SUCCESS,
  REGISTER_VISITOR_FAIL
} from './types.js';

const registerVisitorSuccess = {
  status: 77,
  title: 'Visitor successfully added',
  message: 'Please '
}

const registerVisitorFail = {
  status: 78,
  title: 'Something went wrong',
  message: 'Please try again later'
}

function randomString(length) {
  return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}

export function registerVisitor(visitorObject) {
  const { currentUser } = firebase.auth();
  const { date, ic, name, phone, vehicle, remarks } = visitorObject;
  return (dispatch) => {
    let epochTime = date.getTime(); //just new Date(ep) to get back date object
    firebase.database().ref(`/Visitor/`).push({ //firebase will generate a key
      date: epochTime,
      ic,
      name,
      phone,
      vehicle,
      remarks,
      cancel: false //give user an option to cancel in the future
    }).then((snapshot) => {
        const visitorID = snapshot.key;
        firebase.database().ref(`/ResidentVisitor/${currentUser.uid}`).update({ [visitorID]: true })
          .then(() => dispatch({
            type: REGISTER_VISITOR_SUCCESS,
            payload: registerVisitorSuccess
          }))
      })
      .catch((error) => dispatch({
        type: REGISTER_VISITOR_FAIL,
        payload: registerVisitorFail
      }))
  }
}