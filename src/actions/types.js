export const LISTEN_TO_USER = 'listen_to_user';
export const FIREBASE_USER_EXISTED = 'firebase_user_existed';
export const FIREBASE_USER_NOT_EXISTED = 'firebase_user_not_existed';
export const NEXMO_VERIFY = 'nexmo_verify';
export const NEXMO_CHECK = 'nexmo_check';
export const REGISTER_USER_SUCCESS = 'register_user_success';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const AUTH_FAIL = 'auth_fail';
export const LOGOUT_USER = 'logout_user';

export const GET_USER_PROFILE = 'get_user_profile';
export const STORE_IMAGE_LOCALLY = 'store_image_locally';
export const UPDATE_PROFILE_SUCCESS = 'update_profile_success';
export const UPDATE_PROFILE_FAIL = 'update_profile_fail';

export const REGISTER_VISITOR_SUCCESS = 'register_visitor_success';
export const REGISTER_VISITOR_FAIL = 'register_visitor_fail';