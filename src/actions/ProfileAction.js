import firebase from 'firebase';

import {
  GET_USER_PROFILE,
  STORE_IMAGE_LOCALLY,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAIL
} from './types';

const updateProfileSuccess = {
  status: '12',
  title: 'Success',
  message: 'Your profile has been updated!'
}

const updateProfileFail = {
  status: '13',
  title: 'Something is wrong',
  message: 'Please try again later'
}

export function getUserProfile() {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/Users/${currentUser.uid}`)
      .once('value', (snapshot) => {
        dispatch({
          type: GET_USER_PROFILE,
          payload: snapshot.val()
        });
      });
  }
}

export function storeAvatarLocally(source) {
  return {
    type: STORE_IMAGE_LOCALLY,
    payload: source
  };
}

export function updateProfile(oldPhone, newPhone) {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/Users/${currentUser.uid}`)({
      phone: newPhone
    })
      .then(() => {
        var ref = firebase.database().ref(`ExistingUser`);
        var child = ref.child(`${oldPhone}`)
        child.once('value', (snapshot) => {
          ref.child(`${newPhone}`).set(snapshot.val());
          child.remove();
        })
          .then(() => dispatch({ type: UPDATE_PROFILE_SUCCESS, payload: updateProfileSuccess }))
      })
      .catch((error) => dispatch({ type: UPDATE_PROFILE_FAIL, payload: updateProfileFail }))
  }
}

