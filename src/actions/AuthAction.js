import firebase from 'firebase';
import {
  LISTEN_TO_USER,
  FIREBASE_USER_EXISTED,
  FIREBASE_USER_NOT_EXISTED,
  NEXMO_VERIFY,
  NEXMO_CHECK,
  REGISTER_USER_SUCCESS,
  LOGIN_USER_SUCCESS,
  AUTH_FAIL,
  LOGOUT_USER
} from './types';

const phoneRegistered = {
  status: '55',
  title: 'This phone number has been registered',
  message: 'Please try with another phone number'
}

const phoneNotRegistered = {
  status: '56',
  title: 'Message',
  message: 'This phone number is available'
}

const registerUserSuccess = (dispatch, user) => {
  dispatch({
    type: REGISTER_USER_SUCCESS,
    payload: user
  });
}

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
}

const authFail = (dispatch, error) => {
  dispatch({
    type: AUTH_FAIL,
    payload: error
  });
};

export function listenToUser() {
  return (dispatch) => {
    firebase.auth().onAuthStateChanged(user => {
      dispatch({
        type: LISTEN_TO_USER,
        payload: user
      });
    });
  };
}

export function nexmoVerify(options) {
  console.log(options)
  return (dispatch) => {
    fetch(options.url)
      .then(res => res.json())
      .then(json => {
        console.log(json)
        dispatch({
          type: NEXMO_VERIFY,
          payload: json
        })
      })
      .catch(error => console.log(error))
  }
}

export function nexmoCheck(options) {
  console.log(options)
  return (dispatch) => {
    fetch(options.url)
      .then(res => res.json())
      .then(json => {
        console.log(json)
        dispatch({
          type: NEXMO_CHECK,
          payload: json
        })
      })
      .catch(error => console.log(error))
  }
}

export function checkUserExist(phone) {
  return(dispatch) => {
    firebase.database().ref(`/ExistingUser/`)
      .once('value', snapshot => {
        if(snapshot.hasChild(phone)) {
          dispatch({
            type: FIREBASE_USER_EXISTED,
            payload: phoneRegistered
          });
        } else {
          dispatch({
            type: FIREBASE_USER_NOT_EXISTED,
            payload: phoneNotRegistered
          });
        }
      });
  }
}

// use email as password for future use because there is no need for password for sms verification system
export function registerFirebaseAccount(name, email, phone) {
  console.log('Registering: ' + email)
  return (dispatch) => {
    firebase.auth().createUserWithEmailAndPassword(email, email)
      .then(user => {
        console.log(user)
        registerUserSuccess(dispatch, user)
        createUserRef(name, email, phone)
      })
      .catch((error => authFail(dispatch, error.message)))
  }
}

export function loginUser(email) {
  console.log('Logging in: ' + email)
  return(dispatch) => {
    firebase.auth().signInWithEmailAndPassword(email, email)
      .then(user => loginUserSuccess(dispatch, user))
      .catch(error => authFail(dispatch, error.message));
  };
}

//create user reference onto the database upon registration
function createUserRef(name, email, phone) {
  const { currentUser } = firebase.auth();
  firebase.database().ref(`/Users/${currentUser.uid}`).set({
    name: name,
    email: email,
    phone: phone
  })
  firebase.database().ref(`/ExistingUser/${phone}`).set({
    userID: currentUser.uid,
    name: name,
    email: email,
    phone: phone,
  }).then(() => console.log('Updated Existing User node'))
    .catch((error) => console.log(error.message))
}

export function logoutUser() {
  firebase.auth().signOut();
  return {
    type: LOGOUT_USER
  }
}