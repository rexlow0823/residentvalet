import React, { PropTypes, Component } from 'react';
import {
  View,
  Text,
  Dimensions
} from 'react-native';

import em from './../styles/calculateSize';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';

import { phonecall } from 'react-native-communications';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default class ContactCard extends Component {

  static propTypes = {
    name: PropTypes.string,
    position: PropTypes.string,
    themeColor: PropTypes.string,
    phoneNumber: PropTypes.string,
    status: PropTypes.bool,
    avatar: PropTypes.string
  }

  static defaultProps = {
    name: "John Doe",
    position: "Office Admin",
    themeColor: '#f95a25',
    phoneNumber: '601125571574',
    status: true,
    avatar: ''
  }

  renderProfile = () => {
    if (this.props.avatar !== "") {
      return <Image style={styles.avatarShape} source={{ uri: this.props.avatar }} />
    } else {
      return <Icon name="user" size={30} color={this.props.themeColor} />
    }
  }

  render() {
    const { themeColor, phoneNumber } = this.props;
    const { centerEverything, container, avatarContainer, avatarShape, contentContainer,
     nameStyle, positionStyle, callButton } = styles;

    return (
      <View style={[container]}>
        <View style={[avatarContainer, centerEverything]}>
          <View style={[avatarShape, centerEverything, { borderColor: themeColor }]}>
            {this.renderProfile()}
          </View>
        </View>
        <View style={[contentContainer]}>
          <View style={{ justifyContent: 'center' }}>
            <Text style={nameStyle}>{this.props.name}</Text>
            <Text style={positionStyle}>{this.props.position}</Text>
            <Text style={styles.positionStyle}>{this.props.status ? 'Online' : 'Offline'}</Text>
          </View>
          <View style={[centerEverything]}>
            <Ionicon name="md-call" size={35} color={themeColor} style={callButton} onPress={() => phonecall(phoneNumber, true)} />
          </View>
        </View>
      </View>
    )
  }  

}

const styles = {
  centerEverything: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  testShit: {
    borderWidth: 1,
    borderColor: 'red'
  },
  container: {
    width: deviceWidth*0.9,
    height: deviceHeight*0.15,
    flexDirection: 'row'
  },
  avatarContainer: {
    flex: 3
  },
  contentContainer: {
    flex: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10
  },
  avatarShape: {
    height: deviceWidth*0.16,
    width: deviceWidth*0.16,
    borderRadius: 40,
    borderWidth: 2
  },
  nameStyle: {
    fontSize: em(1.1),
    fontWeight: '700',
    paddingVertical: 4
  },
  positionStyle: {
    fontSize: em(0.95)
  },
  callButton: {
    transform: [{
      "rotate": "260deg"
    }],
  }
}