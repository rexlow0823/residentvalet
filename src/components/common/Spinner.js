import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = ({ size, color, backgroundColor }) => {
  return(
    <View style={[styles.spinnerStyle, backgroundColor]}>
      <ActivityIndicator size={size || 'large'} color={color || 'grey'}/>
    </View>
  )
};

const styles = {
  spinnerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  }
}

export { Spinner };
