import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';

import em from './../../styles/calculateSize';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const ActionButton = ({ propStyle, gradientBegin, gradientEnd, title, icon, onPress }) => {

  const { centerEverything, buttonGradientStyle, titleStyle } = styles;
  return (
    <TouchableOpacity onPress={onPress}>
      <LinearGradient colors={[gradientBegin, gradientEnd]} style={[buttonGradientStyle, centerEverything]}>
        <Ionicons name={icon} size={26} color="#fff" style={{ backgroundColor: 'transparent' }} />
        <Text style={[titleStyle]}>{title}</Text>
      </LinearGradient>
    </TouchableOpacity>
  );
}

const styles = {
  centerEverything: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGradientStyle: {
    height: deviceWidth * 0.42 * 0.55,
    width: deviceWidth * 0.42,
    borderRadius: 10,
    marginVertical: 5
  },
  titleStyle: {
    fontSize: em(0.95),
    color: '#fff',
    fontWeight: '500',
    backgroundColor: 'transparent',
  }
}

export { ActionButton };
