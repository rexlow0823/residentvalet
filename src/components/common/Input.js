import React from 'react';
import {
  View,
  Text,
  TextInput,
  Dimensions
} from 'react-native';

import em from './../../styles/calculateSize';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const Input = ({ propStyle, inputPadding, autoFocus, placeholder, placeholderTextColor, autoCapitalize,
   secureTextEntry, onChangeText, value, keyboardType, maxLength, onEndEditing, propHeight, propWidth, multiline}) => {

  const { inputContainer, inputStyle } = styles;

  return(
    <View style={[inputContainer, inputPadding]}>
      <TextInput
        style={[inputStyle, propStyle, propHeight, propWidth]}
        autoFocus={autoFocus}
        autoCapitalize={autoCapitalize}
        autoCorrect={false}
        multiline={multiline}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangeText}
        value={value}
        keyboardType={keyboardType}
        maxLength={maxLength}
        onEndEditing={onEndEditing}
        underlineColorAndroid='rgba(0,0,0,0)'
      />
    </View>
  );
}

const styles = {
  inputContainer: {
    borderBottomColor: 'rgba(255,255,255,0.5)',
    borderBottomWidth: 2,
    marginBottom: 10
  },
  inputStyle: {
    height: 40,
    width: deviceWidth*0.7,
    color: 'white',
    borderBottomColor: '#fff',
    fontSize: em(1),
  }
}

export { Input };
