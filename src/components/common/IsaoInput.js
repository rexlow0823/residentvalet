import React from 'react';

import Isao from './../lib/Isao';

const IsaoInput = ({ propStyle, inputPadding, label, placeholder, placeholderTextColor,
   secureTextEntry, onChangeText, onFocus, value, keyboardType, maxLength, multiline, height }) => {

  return(
      <Isao
        label={label}
        activeColor={'#323232'}
        passiveColor={'#dadada'}
        onFocus={onFocus}
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        multiline={multiline}
        autoCorrect={false}
        height={height}
      />
  );
}

export { IsaoInput };
