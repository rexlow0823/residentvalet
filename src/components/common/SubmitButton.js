import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions
} from 'react-native';

import em from './../../styles/calculateSize';

const deviceWidth = Dimensions.get('window').width;

const SubmitButton = ({ propStyle, buttonText, onPress, btnColor }) => {
  const { centerEverything, normalButton, normalButtonText } = styles;
  return (
    <View style={[centerEverything]}>
      <TouchableOpacity style={[centerEverything, normalButton, btnColor]} onPress={onPress}>
        <Text style={[normalButtonText]}>{buttonText}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = {
  centerEverything: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  normalButton: {
    width: deviceWidth*0.6,
    height: 50,
    borderRadius: 5,
  },
  normalButtonText: {
    fontSize: em(1.15),
    fontWeight: '500',
    color: '#fff',
    letterSpacing: 1
  }
}

export { SubmitButton };