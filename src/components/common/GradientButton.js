import React from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import em from './../../styles/calculateSize';

const deviceWidth = Dimensions.get('window').width;

const GradientButton = ({ propStyle, buttonText, onPress }) => {
  const { centerEverything, buttonGradientStyle, buttonStyle, buttonTextStyle } = styles;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <LinearGradient colors={['#FF7676', '#F54EA2']} style={[buttonGradientStyle, centerEverything]}>
        <View style={[buttonStyle, centerEverything]} >
          <Text style={buttonTextStyle}>{buttonText}</Text>
        </View>
      </LinearGradient>
    </TouchableWithoutFeedback>
  );
}

const styles = {
  centerEverything: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGradientStyle: {
    height: 28,
    width: deviceWidth * 0.23,
    borderRadius: 5,
    marginLeft: 5
  },
  buttonStyle: {
    height: 25,
    width: deviceWidth * 0.23 - 3,
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderRadius: 3,
  },
  buttonTextStyle: {
    fontSize: em(0.8),
    color: '#F54EA2',
    backgroundColor: 'transparent',
  }
}

export { GradientButton };
