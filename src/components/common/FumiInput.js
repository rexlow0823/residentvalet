import React from 'react';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import Fumi from './../lib/Fumi';

const FumiInput = ({ label, editable, iconName, onChangeText, onFocus, value, keyboardType, multiline, height }) => {
  return(
      <Fumi
        label={label}
        editable={editable}
        iconClass={FontAwesomeIcon}
        iconName={iconName}
        iconColor={'#f95a25'}
        onFocus={onFocus}
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        multiline={multiline}
        autoCorrect={false}
        height={height}
      />
  );
}

export { FumiInput };
