import React, { Component, PropTypes } from 'react';
import {
  Animated,
  View,
  Text,
  Image,
  Platform,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
const wifi = <Icon name="wifi" size={16} />
const child = <Icon name="child" size={16} />
const statusOpen = <Icon name="circle" size={16} color="#76ff03" />
const statusClosed = <Icon name="circle" size={16} color="#d50000" />

import { deviceWidth, itemWidth, slideHeight } from './../styles/calculateSize';
import em from './../styles/calculateSize';

export default class SliderEntry extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    status: PropTypes.bool,
    kidsFriendly: PropTypes.bool,
    operatingStart: PropTypes.string,
    operatingStop: PropTypes.string,
    imageURL: PropTypes.any
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false
    }
  }

  componentWillMount() {
    this.animated = new Animated.Value(0);
  }

  toggleCard = () => {
    this.setState((state) => ({
      open: !state.open
    }), () => {
      const toValue = this.state.open ? 1 : 0;
      Animated.timing(this.animated, {
        toValue,
        duration: 300
      }).start()
    })
  }

  render() {
    const offsetInterpolate = this.animated.interpolate({
      inputRange: [0, 1],
      outputRange: [slideHeight*.3, 0]
    })
    const arrowRotate = this.animated.interpolate({
      inputRange: [0, 1],
      outputRange: ['180deg', '0deg']
    })
    const offsetStyle = {
      transform: [{ translateY: offsetInterpolate }]
    }
    const arrowStyle = {
      transform: [{ rotate: arrowRotate }]
    }

    const opacityStyle = {
      opacity: this.animated
    }

    const { title, subtitle, status, kidsFriendly, imageURL } = this.props;

    const { centerEverything, container, imageStyle, card, header, titleStyle, iconContainer, iconStyle,
      descStyle, arrowContainer, arrow, scrollViewWrap, scrollView } = styles;

    return (
      <TouchableOpacity style={[container]} onPress={() => console.log('press')}>
        <Image style={imageStyle} source={imageURL}>
          <View style={{ flex: .5 }} />
          <Animated.View style={[card, offsetStyle]}>
            <View style={[header]}>
              <View>
                <Text style={titleStyle}>{title.toUpperCase()}</Text>
                <View style={iconContainer}>
                  {/*<Text style={iconStyle}>{status ? statusOpen : statusClosed}</Text>*/}
                  <Text style={iconStyle}>{wifi}</Text>
                  <Text style={iconStyle}>{kidsFriendly ? child : null}</Text>
                </View>
              </View>
              <TouchableOpacity style={[arrowContainer, centerEverything]} onPress={this.toggleCard}>
                <Animated.Text style={[arrow, arrowStyle]}>↓</Animated.Text>
              </TouchableOpacity>
            </View>
            <Animated.View style={[scrollViewWrap, opacityStyle]}>
              <ScrollView contentContainerStyle={scrollView}>
                <Text style={descStyle}>{subtitle}</Text>
              </ScrollView>
            </Animated.View>
          </Animated.View>
        </Image>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  testShit: {
    borderWidth: 1,
    borderColor: 'red'
  },
  centerEverything: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    width: itemWidth,
    height: slideHeight,
  },
  imageStyle: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'cover',
    width: itemWidth,
    height: slideHeight,
    borderRadius: 8
  },
  card: {
    backgroundColor: '#FFF',
    flex: .5,
    paddingHorizontal: 15,
    paddingTop: 5,
    opacity: .85
  },
  header: {
    flex: .25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10
  },
  scrollView: {
    flex: .75,
    paddingTop: 20
  },
  titleStyle: {
    fontSize: em(1.25),
    letterSpacing: 1
  },
  iconContainer: {
    flexDirection: 'row',
    paddingTop: 10
  },
  iconStyle: {
    paddingHorizontal: 3
  },
  descStyle: {
    fontSize: em(1),
  },
  arrowContainer: {
    width: deviceWidth * 0.1,
  },
  arrow: {
    fontSize: em(1.7),
  },
  scrollViewWrap: {
    flex: 1
  },
})