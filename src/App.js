"Firebase.enableLogging(true);"

import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';

import firebase from 'firebase';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk'
import { persistStore, autoRehydrate } from 'redux-persist'
import reducers from './reducers';

import Router from './Router';

export default class ResidentValet extends Component {

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyAPhayOLGDxfGTqy4YHvq0JRBaNclP6N_Q",
      authDomain: "residentvalet.firebaseapp.com",
      databaseURL: "https://residentvalet.firebaseio.com",
      projectId: "residentvalet",
      storageBucket: "residentvalet.appspot.com",
      messagingSenderId: "472016190923"
    })
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk), autoRehydrate());
    persistStore(store, {storage: AsyncStorage})
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}