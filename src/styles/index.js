import { Dimensions, StyleSheet, Platform, NativeModules } from 'react-native';
const { StatusBarManager } = NativeModules;

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
const STATUSBAR_HEIGHT_IOS = STATUSBAR_HEIGHT + 44;
const STATUSBAR_HEIGHT_ANDROID = STATUSBAR_HEIGHT + 30;
const REMAINING_HEIGHT_IOS = deviceHeight - STATUSBAR_HEIGHT_IOS;
const REMAINING_HEIGHT_ANDROID = deviceHeight - STATUSBAR_HEIGHT_ANDROID;

import em from './calculateSize';
import color from './color';

export default StyleSheet.create({
  //
  // STATICS
  //

  //
  // REACT-NATIVE-ROUTER-FLUX
  //
  navigationBarStyle: {
    backgroundColor: color.white,
    borderBottomWidth: 0,
  },
  navTitleStyle: {
    color: color.specialBlack,
    fontWeight: '500'
  },
  navBarButton: {
    width: deviceWidth * 0.15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  //
  // GENERALS
  //
  testShit: {
    borderColor: 'red',
    borderWidth: 2
  },
  centerEverything: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  authContainer: {
    flex: 1,
    marginTop: STATUSBAR_HEIGHT
  },
  bitOfTransparent: {
    backgroundColor: 'rgba(0,0,0,.5)'
  },
  //
  // AUTHS
  //
  spanImage: {
    height: deviceHeight,
    width: deviceWidth,
  },
  statusBarSpan: {
    width: deviceWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    top: 20,
    paddingLeft: 15,
    paddingRight: 15,
    position: 'absolute'
  },
  residentValetUpperContainer: {
    flex: .25,
    width: deviceWidth * 0.8
  },
  residentValetBottomContainer: {
    flex: .75,
    width: deviceWidth,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 40
  },
  residentValetBottomContainerField: {
    justifyContent: 'flex-start'
  },
  residentValetText: {
    fontSize: em(1) * 1.5,
    letterSpacing: 10,
    textAlign: 'center',
    color: '#fff',
    backgroundColor: 'transparent',
  },
  residentValetDesc: {
    fontSize: em(1) * 1.25,
    letterSpacing: 0,
    flexWrap: 'wrap'
  },
  helLight: {
    fontFamily: 'HelveticaNeue-Light',
  },
  loginButton: {
    flexDirection: 'row',
    width: deviceWidth * 0.9,
    height: 50,
    backgroundColor: color.transparent,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 3,
  },
  loginButtonText: {
    fontSize: em(1),
    color: '#fff',
    paddingLeft: 10,
  },
  policyText: {
    fontSize: em(0.85),
    color: '#fff',
    backgroundColor: color.transparent
  },
  nextButtonText: {
    fontSize: em(1.25),
    fontWeight: '500',
    backgroundColor: color.transparent
  },
  container: {
    flex: 1,
    backgroundColor: color.generalBackground2,
    ...Platform.select({
      ios: {
        marginTop: STATUSBAR_HEIGHT_IOS
      },
      android: {
        marginTop: STATUSBAR_HEIGHT_ANDROID
      }
    })
  },
  //
  // CARD
  //
  cardShadow: { //not in used for the moment
    shadowColor: '#FF7676',
    shadowOffset: { width: .25, height: .25 },
    shadowOpacity: .25,
    shadowRadius: 100,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  cardContainer: {
    flexDirection: 'row',
    width: deviceWidth * .9,
    height: REMAINING_HEIGHT_IOS * 0.22,
    backgroundColor: color.white,
    opacity: 1,
    borderRadius: 10,
    shadowColor: color.gray,
    shadowOffset: { width: 1, height: 10 },
    shadowOpacity: .15,
    shadowRadius: 10,
  },
  cardAvatar: {
    flex: .4,
  },
  avatarShape: {
    height: 80,
    width: 80,
    borderRadius: 40,
  },
  avatarBorder: {
    borderColor: color.lightGray,
    borderWidth: 1,
  },
  cardContentContainer: {
    flex: .6,
    flexDirection: 'column'
  },
  cardContent: {
    flex: .68,
    justifyContent: 'center',
    paddingLeft: 10
  },
  cardButtonContainer: {
    flex: .32,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  transparentText: {
    backgroundColor: 'transparent'
  },
  residentName: {
    fontSize: em(1.2),
    fontWeight: 'bold',
    paddingBottom: 1,
    color: color.specialBlack,
    backgroundColor: 'transparent'
  },
  unitStyle: {
    fontSize: em(0.95),
    paddingBottom: 2,
    color: color.specialBlack,
  },
  indicator: {
    flex: 0.005,
    width: deviceWidth * 0.75,
    backgroundColor: color.lightGray,
    marginTop: 15
  },
  infoZone: {
    flex: .995
  },
  // 
  // ACTION
  //
  actionButtonContainer: {
    height: deviceHeight * 0.42,
    width: deviceWidth * 0.9,
    marginTop: 20
  },
  actionButtonContentContainer: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: deviceWidth * 0.9,
    marginTop: 10
  },
  //
  // EIDT PROFILE
  //
  epUpperContainer: {
    flex: .25,
    backgroundColor: color.white,
    paddingVertical: 20
  },
  epBottomContainer: {
    flex: .75
  },
  epName: {
    fontSize: em(1.15),
    fontWeight: '500',
    letterSpacing: 2,
    marginTop: 15,
  },
  epResidenceName: {
    fontSize: em(0.8),
    letterSpacing: 1,
    marginTop: 5
  },
  epHeader: {
    flex: .05,
    justifyContent: 'center',
    paddingLeft: 25,
    paddingTop: 20,
    backgroundColor: color.white,
  },
  epHeaderText: {
    fontSize: em(1),
    fontWeight: '500',
    color: color.gray
  },
  epContent: {
    flex: .95,
    backgroundColor: color.white,
    padding: 10
  },
  visitorHeader: {
    fontSize: em(1),
    color: color.gray,
    fontWeight: '500',
    paddingVertical: 3
  },
  visitorHeaderIcon: {
    fontSize: em(2.5),
    paddingVertical: 2
  },
  calendarView: {
    height: 48,
    width: deviceWidth * 0.9,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  //
  // FACILITIES CARIUSEL
  //
  sliderWidth: {

  },
  itemWidth: {
    width: deviceWidth * 0.5
  },
  contentContainerCustomStyle: {

  },
  //
  // ABOUT US
  //
  auParallel: {
    flex: 1,
  },
  auArrowContainer: {
    width: deviceWidth*0.12,
    height: deviceWidth*0.12,
    backgroundColor: color.transparent,
    position: 'absolute',
    top: 30,
    right: 20
  },
  auArrow: {
    fontSize: em(1.6),
    color: color.white
  },
  auParallelBackground: {
    position: 'absolute',
    top: 0,
    width: deviceWidth,
    backgroundColor: 'rgba(0,0,0,.4)',
    height: deviceHeight*0.45
  },
  auParallelImage: {
    width: deviceWidth,
    height: deviceHeight*0.45,
    resizeMode: 'cover',
  },
  auParallelHeader: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: deviceHeight*0.05,
  },
  auMiddle: {
    flex: .5,
    width: deviceWidth,
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  auBottom: {
    flex: .2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: deviceWidth,
  },
  auTitleContainer: {
    flex: .33,
    width: deviceWidth * 0.7,
    borderBottomWidth: 0.5,
    borderColor: color.lightGray
  },
  auLinkTitle: {
    fontSize: em(1.25),
    letterSpacing: 2
    // fontWeight: '500'
  }
})