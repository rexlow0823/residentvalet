export default color = {
  white: '#fff',
  black: '#000',
  gray: 'gray',
  lightGray: '#979797',
  specialBlack: '#323232',
  specialRed: '#FF7676',
  transparent: 'transparent',
  transparentRGB: 'rgba(0,0,0,0)',
  generalBackground: '#F5F5F5',
  generalBackground2: '#EEF1F6'
}