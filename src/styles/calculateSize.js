import { Dimensions } from 'react-native';

export const deviceWidth = Dimensions.get('window').width;
export const deviceHeight = Dimensions.get('window').height;

// Calculating ratio from iPhone breakpoints
export const ratioX = deviceWidth < 375 ? (deviceWidth < 320 ? 0.75 : 0.875) : 1 ;
export const ratioY = deviceHeight < 568 ? (deviceHeight < 480 ? 0.75 : 0.875) : 1 ;

// Set our base font size value
const base_unit = 16;

// Simulating EM by changing font size according to Ratio
const unit = base_unit * ratioX;

export default function em(value) {
  return (unit * value);
}

function wp (percentage) {
    const value = (percentage * deviceWidth) / 100;
    return Math.round(value);
}

export const slideHeight = deviceHeight * 0.65;
export const slideWidth = wp(75);

export const sliderWidth = deviceWidth;
export const itemHorizontalMargin = wp(2);
export const itemWidth = slideWidth + itemHorizontalMargin * 2;