import React, { Component } from 'react';
import {
  Image,
  LayoutAnimation,
  Text,
  View
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as actions from './../actions';

import styles from './../styles';

import {
  Spinner
} from './../components/common'

class SplashScreen extends Component {

  constructor(props) {
    super(props);
  }

  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  componentDidMount() {
    this.processAuth(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.processAuth(nextProps)
  }

  processAuth(props) {
    if(props.auth.user != null) {
      if(props.auth.user.uid) {
        Actions.main({ type: 'reset' });
      } else {
        this.wait(1000);
        Actions.preLogin({ type: 'reset' });
      }
    }
  }

  wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
      end = new Date().getTime();
    }
  }
  
  render() {
    const { testShit, spanImage, residentValetUpperContainer, residentValetBottomContainer, residentValetText, centerEverything, authContainer, helLight } = styles;
    return (
      <Image 
        resizeMode="cover"
        style={[spanImage]}
        source={require('./../images/House.jpg')}>
        <View style={[centerEverything, authContainer]}>
          <View style={[residentValetUpperContainer, centerEverything]}>
            <Text style={[residentValetText, helLight]}>RESIDENT</Text>
            <Text style={[residentValetText, helLight]}>VALET</Text>
          </View>
          <View style={residentValetBottomContainer}>
            <Spinner size="small" color="#fff" />
          </View>
        </View>
      </Image>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, actions)(SplashScreen);