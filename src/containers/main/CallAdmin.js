import React, { PropTypes, Component } from 'react';
import {
  Text,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './../../styles';
import ContactCard from './../../components/ContactCard';

class CallAdmin extends Component {

  render() {
    const { centerEverything, container, epUpperContainer, epBottomContainer, visitorHeader } = styles;
    return (
      <View style={[container, centerEverything, { backgroundColor: '#fff' }]}>
        <View style={[epUpperContainer, centerEverything]}>
          <Icon name="users" size={40} style={{ paddingVertical: 5 }} color="gray" />
          <Text style={visitorHeader}>Have an inquiry? Call us!</Text>
        </View>
        <View style={[epBottomContainer]}>
          <ContactCard themeColor="#2979ff" phoneNumber="60136639817" />
          <ContactCard position="Duty Manager" themeColor="#f50057" />
          <ContactCard position="Head of Guard" status={false} />
        </View>
      </View>
    );
  }
}


export default CallAdmin;