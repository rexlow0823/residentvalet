import React, { PropTypes, Component } from 'react';
import {
  Alert,
  Text,
  Dimensions,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import { registerVisitor } from './../../actions';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DatePicker from 'react-native-datepicker'

import Icon from 'react-native-vector-icons/FontAwesome';
const calendarIcon = (<Icon name="calendar-o" size={20} color="#FE421D" />)

import styles from './../../styles';
const deviceWidth = Dimensions.get('window').width;

import {
  FumiInput,
  SubmitButton,
  Spinner
} from './../../components/common';


class RegisterVisitor extends Component {

  static PropTypes = {

  }

  constructor(props) {
    super(props)
    this.state = {
      inviting: false,
      date: new Date(),
      name: '',
      ic: '',
      phone: '',
      vehicle: '',
      remarks: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    this.processProps(nextProps)
  }

  processProps(props) {
    console.log(props)
  }

  inviteVisitor = () => {
    const { name, ic, phone, vehicle, date, remarks } = this.state;
    if (name === '' || ic === '') {
      Alert.alert('Message', 'Please fill in the required (*) fields');
    } else {
      this.setState({ inviting: !this.state.inviting })
      this.props.registerVisitor(this.state)
    }
  }

  renderInviteButton = () => {
    const btnColor = (this.state.name && this.state.ic ? { backgroundColor: '#FE421D' } : { backgroundColor: '#a3a3a3' })
    if (this.state.inviting === false) {
      return <SubmitButton buttonText="INVITE" onPress={this.inviteVisitor} btnColor={btnColor} />
    } else {
      return <Spinner color="#000" size="small" backgroundColor={{ backgroundColor: '#fff' }} />
    }
  }

  render() {
    const { centerEverything, container, epUpperContainer, epBottomContainer, visitorHeaderIcon, visitorHeader, calendarView } = styles;
    return (
      <KeyboardAwareScrollView>
        <View style={[container]}>
          <View style={[epUpperContainer, centerEverything]}>
            <Text style={visitorHeaderIcon}>🍹</Text>
            <Text style={visitorHeader}>Invite your friends over for a party</Text>
            <Text style={visitorHeader}>The more the merrier!</Text>
          </View>
          <View style={[epBottomContainer, { padding: 10, backgroundColor: '#fff' }]}>
            <View style={[calendarView]}>
              {calendarIcon}
              <DatePicker
                style={[]}
                showIcon={false}
                date={this.state.date}
                mode="date"
                placeholder="select a date"
                format="MMM D YYYY"
                minDate={new Date()}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  placeholderText: {
                    color: '#a3a3a3'
                  },
                  dateInput: {
                    padding: 7,
                    borderWidth: 0
                  },
                  dateText: {
                    fontSize: 18,
                    fontFamily: 'Arial',
                    color: '#FE421D'
                  }
                }}
                onDateChange={(date) => { this.setState({ date }) }}
              />
            </View>
            <FumiInput
              label="Visitor Name*"
              iconName="user"
              onChangeText={name => this.setState({ name })}
              value={this.state.name} />
            <FumiInput
              label="Identity Number*"
              iconName="user"
              onChangeText={ic => this.setState({ ic })}
              value={this.state.ic} />
            <FumiInput
              label="Phone number"
              iconName="phone"
              keyboardType="numeric"
              onChangeText={phone => this.setState({ phone })}
              value={this.state.phone} />
            <FumiInput
              label="Plate Number"
              iconName="car"
              onChangeText={vehicle => this.setState({ vehicle })}
              value={this.state.vehicle} />
            <FumiInput
              label="Remarks"
              iconName="book"
              multiline
              height={100}
              onChangeText={remarks => this.setState({ remarks })}
              value={this.state.remarks} />
            {this.renderInviteButton()}
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    api: state.api
  }
}

export default connect(mapStateToProps, { registerVisitor })(RegisterVisitor);