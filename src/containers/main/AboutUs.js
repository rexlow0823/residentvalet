import React, { Component } from 'react';
import {
  Image,
  Linking,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import ParallaxScrollView from 'react-native-parallax-scroll-view';

import { email } from 'react-native-communications';
import Icon from 'react-native-vector-icons/FontAwesome';

import { deviceWidth, deviceHeight } from './../../styles/calculateSize';
import styles from './../../styles';
import color from './../../styles/color';

class AboutUs extends Component {

  constructor(props) {
    super(props)
    this.state = {
      management: ['qiweilow950823@gmail.com'],
      subject: 'Condominium Inquiry',
      website: "https://www.google.com",
      facebook: "https://www.facebook.com",
      twitter: "https://www.twitter.com"
    }
  }

  emailToManagement = () => {
    email(this.state.management, null, null, this.state.subject, null)
  }

  openWebsite(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    });
  }

  _renderBackground() {
    return (
      <View key="background">
        <Image style={styles.auParallelImage} source={require('./../../images/House.jpg')} />
        <View style={styles.auParallelBackground} />
      </View>
    )
  }

  _renderForeground() {
    const { centerEverything, auParallelHeader, auLinkTitle, auArrowContainer, auArrow } = styles;
    return (
      <View style={[auParallelHeader, centerEverything]}>
        <TouchableOpacity style={[auArrowContainer, centerEverything]} onPress={() => Actions.pop()}>
          <Text style={auArrow}>→</Text>
        </TouchableOpacity>
        <Icon name="skyatlas" size={55} color={color.white} />
        <Text style={[auLinkTitle, { color: '#FFF' }]}>DIAMOND RESIDENCE</Text>
      </View>
    )
  }

  render() {
    const { centerEverything, auParallel, auMiddle, auBottom, auTitleContainer, auLinkTitle } = styles;
    return (
      <ParallaxScrollView
        style={[auParallel]}
        parallaxHeaderHeight={deviceHeight*0.45}
        renderBackground={this._renderBackground}
        renderForeground={this._renderForeground}
      >
        <View style={[centerEverything, { backgroundColor: '#fff', height: deviceHeight*0.55 }]}>
          <View style={[auMiddle]}>
            <TouchableOpacity style={[auTitleContainer, centerEverything]}>
              <Text style={[auLinkTitle]}>NEWS</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[auTitleContainer, centerEverything]}>
              <Text style={[auLinkTitle]}>MEDIA</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[auTitleContainer, centerEverything, { borderBottomWidth: 0 }]}>
              <Text style={[auLinkTitle]}>LOCATION</Text>
            </TouchableOpacity>
          </View>
          <View style={[auBottom]}>
            <Icon name="globe" size={22} onPress={() => this.openWebsite(this.state.website)} />
            <Icon name="facebook-official" size={22} onPress={() => this.openWebsite(this.state.facebook)} />
            <Icon name="twitter" size={22} onPress={() => this.openWebsite(this.state.twitter)} />
            <Icon name="envelope" size={22} onPress={this.emailToManagement} />
          </View>
        </View>
      </ParallaxScrollView>
    );
  }
}


export default AboutUs;