import React, { PropTypes, Component } from 'react';
import {
  Alert,
  Image,
  Dimensions,
  Text,
  View,
  ScrollView
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { getUserProfile, logoutUser } from './../../actions';

import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ContactsWrapper from 'react-native-contacts-wrapper';
import { textWithoutEncoding } from 'react-native-communications';

import {
  ActionButton,
  GradientButton
} from './../../components/common';

import styles from './../../styles';

const deviceHeight = Dimensions.get('window').height;

const CONTENT = [
  {
    title: 'John Doe',
    unitNo: 'A 13A 10',
    residence: 'The Residence'
  },
]

class Home extends Component {

  static propTypes = {
    getUserProfile: PropTypes.func.isRequired,
    logoutUser: PropTypes.func.isRequired,
    name: PropTypes.string,
    unit: PropTypes.string,
    residence: PropTypes.string,
    address: PropTypes.string,
  }

  static defaultProps = {
    profile: {
      name: 'Your name',
      unit: 'House Unit',
      residence: 'Residence Name',
      address: ''
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      activeSection: false, // make it closed by default
      managementNumber: '0136639817',
    }
  }

  componentWillMount() {
    this.props.getUserProfile()
  }

  componentWillReceiveProps(nextProps) {
    this.processProps(nextProps)
  }

  processProps(props) {
    // const { avatar, email, name, phone, residence, unit } = props.profile;
    // this.setState({
    //   avatar,
    //   email,
    //   name,
    //   phone,
    //   residence,
    //   unit,
    // })
  }

  logoutHelper() {
    Alert.alert('Sign Out', 'Are you sure?',
      [
        { text: 'No', onPress: () => console.log('cancel log out') },
        {
          text: 'Yes', onPress: () => {
            this.props.logoutUser();
            Actions.auth({ type: 'reset' })
          }
        }
      ]
    )
  }

  _setSection(activeSection) {
    this.setState({ activeSection });
  }

  _renderHeader(section, index, isActive) {
    const { centerEverything, cardContainer, cardAvatar, cardContentContainer, avatarShape, avatarBorder, cardContent, cardButtonContainer,
      transparentText, residentName, unitStyle } = styles;
    return (
      <Animatable.View
        duration={500}
        transition="backgroundColor">
        <View style={[cardContainer, { borderBottomLeftRadius: (isActive ? 0 : 10), borderBottomRightRadius: (isActive ? 0 : 10) }]}>
          <View style={[cardAvatar, centerEverything]}>
            <View style={[avatarShape, avatarBorder, centerEverything]}>
              {
                (() => {
                  switch (this.props.profile.avatar) {
                    case null:
                      return <Ionicons name="md-person" size={36} />;
                    default:
                      return <Image style={avatarShape} source={{ uri: this.props.profile.avatar }} />
                  }
                })()
              }
            </View>
          </View>
          <View style={[cardContentContainer]}>
            <View style={[cardContent]}>
              <Text style={[transparentText, residentName]}>{this.props.profile.name}</Text>
              <Text style={[transparentText, unitStyle]}>{this.props.profile.unit}</Text>
              <Text style={[transparentText, unitStyle]}>{this.props.profile.residence}</Text>
            </View>
            <View style={[cardButtonContainer]}>
              <GradientButton buttonText="Profile" onPress={() => Actions.editProfile()} />
              <GradientButton buttonText="Sign Out" onPress={() => this.logoutHelper()} />
            </View>
          </View>
        </View>
      </Animatable.View>
    );
  }

  _renderContent(section, index, isActive) {
    const { cardContainer, centerEverything, indicator, infoZone } = styles;
    return (
      <Animatable.View
        duration={500}
        style={[cardContainer, centerEverything, { flexDirection: 'column', borderTopLeftRadius: (isActive ? 0 : 10), borderTopRightRadius: (isActive ? 0 : 10) }]}
        transition="backgroundColor">
        <View style={[indicator]} />
        <View style={[centerEverything, infoZone]}>
          <Animatable.Text duration={300} easing="ease-out" animation={isActive ? 'zoomIn' : undefined} style={{ backgroundColor: 'transparent' }}>{section.residence}</Animatable.Text>
        </View>
      </Animatable.View>
    );
  }

  dynamicHeight() {
    if (this.state.activeSection === 0) {
      return {
        height: deviceHeight * 0.43
      }
    } else {
      return {
        height: deviceHeight * 0.6
      }
    }
  }

  shareAddress = () => {
    let address = `${this.props.profile.unit}, ${this.props.profile.address}`
    ContactsWrapper.getContact()
      .then((contact) => {
        textWithoutEncoding(contact.phone, address)
      })
      .catch((error) => {
        console.log("ERROR CODE: ", error.code);
        console.log("ERROR MESSAGE: ", error.message);
      })
  }

  render() {
    const { container, actionButtonContainer, actionButtonContentContainer, residentName } = styles;

    return (
      <View style={[container, { alignItems: 'center', paddingTop: 20 }]}>
        <Accordion
          underlayColor="transparent"
          activeSection={this.state.activeSection}
          sections={CONTENT}
          renderHeader={this._renderHeader.bind(this)}
          renderContent={this._renderContent}
          duration={500}
          onChange={this._setSection.bind(this)}
        />
        <View style={[actionButtonContainer, this.dynamicHeight()]}>
          <Text style={[residentName]}>Manage Your Home</Text>
          <ScrollView
            pagingEnabled={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={[actionButtonContentContainer]}>
            <ActionButton gradientBegin="#8fff76" gradientEnd="#25c900" icon="ios-call" title="Call Management" onPress={() => Actions.callAdmin()} />
            <ActionButton gradientBegin="#FF7676" gradientEnd="#F54EA2" icon="ios-people" title="Register Visitor" onPress={() => Actions.registerVisitor()} />
            <ActionButton gradientBegin="#42E695" gradientEnd="#3BB2B8" icon="md-card" title="Pay Bills" />
            <ActionButton gradientBegin="#FCE38A" gradientEnd="#F38181" icon="md-navigate" title="Share Address" onPress={this.shareAddress} />
            <ActionButton gradientBegin="#17EAD9" gradientEnd="#6078EA" icon="md-game-controller-b" title="Book Facilities" onPress={() => Actions.bookFacilities()} />
          </ScrollView>
        </View>
      </View>
    );
  }

}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    profile: state.profile
  }
}

export default connect(mapStateToProps, { getUserProfile, logoutUser })(Home);