import React, { Component } from 'react';
import { Text, View } from 'react-native';

import Carousel from 'react-native-snap-carousel';
import SlideEntry from './../../components/SlideEntry';
import facilities from './../../statics/facilities';

import styles from './../../styles';
import em, { sliderWidth, itemWidth } from './../../styles/calculateSize';

class BookFacilities extends Component {

  getSlides = (entries) => {
    if (!entries) {
      return false;
    }
    return entries.map((entry, index) => <SlideEntry key={index} {...entry} />)
  }

  render() {
    return (
      <View style={[styles.container, { backgroundColor: '#fff' } ]}>
        <View style={{ flex: .1, justifyContent: 'flex-end', alignItems: 'center' }}>
          <Text style={{ fontSize: em(1.25), fontWeight: '500' }}>Choose a facility</Text>
        </View>
        <Carousel
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          firstItem={1}
          inactiveSlideScale={0.95}
          inactiveSlideOpacity={0.5}
          enableMomentum={false}
          containerCustomStyle={[{ flex: .9, backgroundColor: 'white' }]}
          contentContainerCustomStyle={[{ paddingTop: 15 }]}
          showsHorizontalScrollIndicator={false}
          snapOnAndroid={true}
          removeClippedSubviews={false}>
          {this.getSlides(facilities)}
        </Carousel>
      </View>
    );
  }
}

export default BookFacilities;