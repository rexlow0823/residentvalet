import React, { PropTypes, Component } from 'react';
import {
  Alert,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import * as actions from './../../actions';

import Ionicons from 'react-native-vector-icons/Ionicons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';

import styles from './../../styles';

import {
  FumiInput,
  Spinner,
  SubmitButton
} from './../../components/common';


class EditProfile extends Component {

  static PropTypes = {
    name: PropTypes.string,
    email: PropTypes.string,
    storeAvatar: PropTypes.func,
    updateProfile: PropTypes.func,
    uploadImageSuccess: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      name: this.props.profile.name,
      email: this.props.profile.email,
      phone: this.props.profile.phone,
      unit: this.props.profile.unit,
      residence: this.props.profile.residence,
      address: this.props.profile.address,
      avatar: this.props.profile.avatar,
      diff: false
    }
  }

  componentWillReceiveProps(nextProps) {
    this.processProps(nextProps)
  }

  processProps(props) {
    if (props.profile.avatar) {
      this.setState({
        avatar: props.profile.avatar
      });
    }
  }

  // submitUpdate = () => {
  //   if(this.state.phone === this.props.profile.phone) {
  //     Alert.alert('No changes have been made')
  //   } else {
  //     Alert.alert('Submit Update?', '',
  //     [
  //       {text: 'Cancel', onPress: () => console.log('cancel pressed')},
  //       {text: 'Ok', onPress: () => this.props.updateProfile(this.props.profile.phone, this.state.phone)},
  //     ])
  //   }
  // }
  
  imagePickerHelper = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        
      },
      allowsEditing: true,
      mediaType: 'photo'
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        let avatar = { uri: 'data:image/jpeg;base64,' + response.data, isStatic: true };

        this.setState({ avatar: '' }) // return spinner
        
        //store avatar locally
        this.props.storeAvatarLocally(avatar)

        //upload image to 
        // uploadImage(response.uri)
        //   .then(url => this.props.uploadImageSuccess(url))
        //   .catch(error => console.log(error))
      }
    });
  }

  render() {
    const { testShit, centerEverything, container, epUpperContainer, epBottomContainer, avatarShape, avatarBorder, epHeaderText, epName,
    epResidenceName, epHeader, epContent} = styles;
    return (
      <KeyboardAwareScrollView>
          <View style={[container]}>
            <View style={[epUpperContainer, centerEverything]}>
              <TouchableOpacity onPress={this.imagePickerHelper}>
                <View style={[avatarShape, avatarBorder, centerEverything]}>
                  {
                    (() => {
                      switch (this.state.avatar) {
                        case null:
                          return <Ionicons name="md-person-add" size={34} color="gray" />;
                        case '':
                          return <Spinner size="small" />
                        default:
                          return <Image style={avatarShape} source={{ uri: this.state.avatar }} />
                      }
                    })()
                  }
                </View>
              </TouchableOpacity>
              <Text style={[epName]}>{this.state.name.toUpperCase()}</Text>
              <Text style={[epResidenceName]}>{this.state.residence.toUpperCase()}</Text>
            </View>
            <View style={[epBottomContainer]}>
              <View style={[epHeader]}>
                <Text style={epHeaderText}>Contact Information</Text>
              </View>
              <View style={[epContent]}>
                <FumiInput 
                  label="Email" 
                  editable={false}
                  iconName="envelope" 
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email} />
                <FumiInput 
                  label="Phone Number" 
                  editable={false}
                  iconName="phone" 
                  keyboardType="numeric" 
                  onChangeText={phone => this.setState({ phone })}
                  value={this.state.phone} />
                <FumiInput 
                  label="Unit No" 
                  editable={false}
                  iconName="home"
                  onChangeText={unit => this.setState({ unit })}
                  value={this.state.unit} />
                <FumiInput 
                  label="Home Address" 
                  editable={false}
                  iconName="institution" 
                  multiline
                  height={100}
                  onChangeText={address => this.setState({ address })}
                  value={this.state.address} />
                {/*<SubmitButton buttonText="SUBMIT UPDATE" />*/}
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile
  }
}

export default connect(mapStateToProps, actions)(EditProfile);