import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import styles from './../../styles';

import {
  
} from './../../components/common';

import Ionicons from 'react-native-vector-icons/Ionicons';

class PreLogin extends Component {

  render() {
    const { spanImage, residentValetUpperContainer, residentValetBottomContainer, residentValetText, centerEverything, authContainer,
       helLight, loginButton, loginButtonText } = styles;
    return (
      <Image 
        resizeMode="cover"
        style={[spanImage]}
        source={require('./../../images/House.jpg')}>
        <View style={[centerEverything, authContainer]}>
          <View style={[residentValetUpperContainer, centerEverything]}>
            <Text style={[residentValetText, helLight]}>RESIDENT</Text>
            <Text style={[residentValetText, helLight]}>VALET</Text>
          </View>
          <View style={[residentValetBottomContainer]}>
            <TouchableOpacity 
              style={[loginButton, centerEverything]}
              onPress={() => Actions.login()}>
              <Ionicons name="ios-phone-portrait-outline" color="#fff" size={28} />
              <Text style={[loginButtonText]}>LOG IN WITH MOBILE NUMBER</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Image>
    );
  }
}

export default PreLogin;