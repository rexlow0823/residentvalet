import React, { Component } from 'react';
import {
  Alert,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as actions from './../../actions';

import styles from './../../styles';

import { Input, Spinner } from './../../components/common';

import Ionicons from 'react-native-vector-icons/Ionicons';

const dismissKeyboard = require('dismissKeyboard')

class NexmoVerify extends Component {

  constructor(props) {
    super(props)
    this.state = {
      request_id: '',
      nexmoCode: '',
      phoneRegistered: false //default as user is new
    }
  }

  componentDidMount() {
    if (this.props.auth.message.status == '55') { // old user comes back
      this.setState({ phoneRegistered: true })
    }
  }

  componentWillUpdate(prevState, nextState) {
    if (this.state.nexmoCode.length === 3) { // all 4 digits have been entered
      this.nexmoCheck(nextState.nexmoCode)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.processAuth(nextProps)
  }

  processAuth(props) {
    console.log(props)
    const { status, event_id, request_id } = props.auth.message;
    if (status === '10') {
      Alert.alert('Tried too many times', 'Please try again in a little while',
        [
          { text: 'Ok', onPress: () => Actions.pop() }
        ])
    } else if (status === '3') {
      Alert.alert('Oops', 'Please enter a phone number',
        [
          { text: 'Ok', onPress: () => Actions.pop() }
        ])
    } else if (status === '0' && event_id) {
      if (!this.state.phoneRegistered) {
        Alert.alert('Welcome to Resident Valet', 'We hope you will enjoy the app!',
          [
            {
              text: 'Ok', onPress: () => {
                this.props.registerFirebaseAccount(this.props.name, this.props.email, this.props.phone);
              }
            }
          ])
      } else {
        Alert.alert('Hi There!', 'Its good to be have you back!',
          [
            {
              text: 'Ok', onPress: () => {
                this.props.loginUser(this.props.email);
              }
            }
          ])
      }
    } else if (status === '0') { // verified request_id from nexmo
      this.setState({ request_id: request_id })
    } else if (status === '6' || status === '16' || status === '17') { // errors
      Alert.alert('Error', 'The verification code entered is incorrect!',
        [
          { text: 'Ok', onPress: () => this.setState({ nexmoCode: '' }) }
        ]
      )
    } else if (status === '2') {
      console.log('Your request is incomplete and missing the mandatory parameter: request_id')
    }
  }

  nexmoCheck(nexmoCode) {
    var options = {
      url: 'https://api.nexmo.com/verify/check/json?api_key=57a490e8&api_secret=a10fd42524328175&request_id=' + this.state.request_id + '&code=' + nexmoCode,
    }
    this.props.nexmoCheck(options);
  }

  renderDigit() {
    if (this.state.nexmoCode.length === 4) {
      return <Spinner size="small" color="#fff" />
    } else {
      return (
        <Input
          autoFocus
          maxLength={4}
          placeholder="Enter 4-digit verification code"
          placeholderTextColor="#fff"
          keyboardType="numeric"
          onChangeText={nexmoCode => this.setState({ nexmoCode })}
          value={this.state.nexmoCode}
          propStyle={{ textAlign: 'center', fontWeight: '600' }}
        />
      )
    }
  }

  render() {
    const { spanImage, statusBarSpan, residentValetUpperContainer, residentValetBottomContainer, residentValetBottomContainerField,
      residentValetText, residentValetDesc, centerEverything, authContainer, helLight } = styles;
    return (
      <TouchableWithoutFeedback onPress={() => dismissKeyboard()}>
        <Image
          resizeMode="cover"
          style={[spanImage]}
          source={require('./../../images/House.jpg')}>
          <View style={[centerEverything, authContainer, { backgroundColor: 'rgba(0,0,0,.5)' }]}>
            <View style={[statusBarSpan]}>
              <Ionicons
                name="ios-arrow-round-back"
                color="#fff" size={40}
                style={{ backgroundColor: 'transparent' }}
                onPress={() => Actions.pop()} />
            </View>

            <View style={[residentValetUpperContainer, centerEverything, { marginTop: 45 }]}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={[residentValetText, residentValetDesc, helLight]}>Please enter the 4-digit activation code that was sent to the number below</Text>
              </View>
              <Text style={[residentValetText, { fontWeight: '500', marginTop: 10, letterSpacing: 1 }]}>{this.props.phone}</Text>
            </View>
            <View style={[residentValetBottomContainer, residentValetBottomContainerField, { paddingTop: 20 }]}>
              {this.renderDigit()}
            </View>
          </View>
        </Image>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, actions)(NexmoVerify);