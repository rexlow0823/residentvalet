import React, { Component } from 'react';
import {
  Alert,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as actions from './../../actions';

const deviceWidth = Dimensions.get('window').width;

import styles from './../../styles';

import {
  Input,
  Spinner
} from './../../components/common';

import Ionicons from 'react-native-vector-icons/Ionicons';

const dismissKeyboard = require('dismissKeyboard')

class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      phone: '',
      processing: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.processAuth(nextProps)
  }

  processAuth(props) {
    if(props.auth.message.status === '55' || props.auth.message.status === '56') {
      this.setState({ processing: false });
      this.nexmoVerify()
    }
  }

  nextButtonStyle() {
    const { name, email, phone } = this.state;
    if (!name || !email || !phone) {
      return { color: 'grey' }
    } else {
      return { color: '#fff' }
    }
  }

  renderSpinner() {
    if(this.state.processing) {
      return (
        <Spinner size="small" color="#fff" />
      )
    } else {
      return;
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateFirebase() {
    if(this.validateEmail(this.state.email)) {
      this.setState({ processing: true })
      this.props.checkUserExist(this.state.phone)
    } else {
      Alert.alert('Invalid Email Format', 'Please enter a proper email adderss')
    }
  }

  nexmoVerify() {
    var options = {
      url: 'https://api.nexmo.com/verify/json?api_key=57a490e8&api_secret=a10fd42524328175&number=' + this.state.phone + '&brand=Resident Valet',
    }
    this.props.nexmoVerify(options);
    Actions.nexmoVerify(this.state);
  }

  render() {
    const { spanImage, bitOfTransparent, statusBarSpan, residentValetUpperContainer, residentValetBottomContainer, residentValetBottomContainerField, residentValetText, residentValetDesc, centerEverything, authContainer,
      helLight, policyText, nextButtonText } = styles;
    return (
      <TouchableWithoutFeedback onPress={() => dismissKeyboard()}>
        <Image
          resizeMode="cover"
          style={[spanImage]}
          source={require('./../../images/House.jpg')}>
          <View style={[centerEverything, authContainer, bitOfTransparent]}>
            <View style={[statusBarSpan]}>
              <Ionicons
                name="ios-arrow-round-back"
                color="#fff" size={40}
                style={{ backgroundColor: 'transparent' }}
                onPress={() => Actions.pop()} />
              <TouchableOpacity onPress={() => this.validateFirebase()}>
                <Text style={[nextButtonText, this.nextButtonStyle()]}>Next</Text>
              </TouchableOpacity>
            </View>

            <View style={[residentValetUpperContainer, centerEverything, { flexDirection: 'row' }]}>
              <Text style={[residentValetText, residentValetDesc, helLight]}>Please enter your details.</Text>
            </View>

            <View style={[residentValetBottomContainer, residentValetBottomContainerField]}>
              <Input placeholder="Full Name" placeholderTextColor="#fff" value={this.state.name} onChangeText={(name) => this.setState({ name })} autoFocus />
              <Input placeholder="Email" placeholderTextColor="#fff" value={this.state.email} onChangeText={(email) => this.setState({ email })} keyboardType="email-address" autoCapitalize={'none'} />
              <Input placeholder="Phone Number" placeholderTextColor="#fff" value={this.state.phone} onChangeText={(phone) => this.setState({ phone })} keyboardType="phone-pad" />
              <View style={{ flexDirection: 'row', width: deviceWidth * 0.7 }}>
                <Text style={[policyText]}>By registering, I have read and agreed to the Terms of Use and the Privacy Policy</Text>
              </View>
              {this.renderSpinner()}
            </View>
          </View>
        </Image>
      </TouchableWithoutFeedback>

    );
  }
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, actions)(Login);