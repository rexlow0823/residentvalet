export default facilites = [
  {
    title: 'Swimming Pool',
    subtitle: 'The swimming pool is open for all residence. Operation hour starts from 6am to 11pm',
    status: true,
    kidsFriendly: true,
    imageURL: require('./../images/facilities/pool.jpeg')
  },
  {
    title: 'The Gym',
    subtitle: 'The Gym is open for all residence. Operation hour starts from 6am to 11pm',
    status: false,
    kidsFriendly: false,
    imageURL: require('./../images/facilities/gym.jpg')
  },
  {
    title: 'Conference Room',
    subtitle: 'The conference room is open for all residence. Operation hour starts from 9am to 11pm',
    status: true,
    kidsFriendly: false,
    imageURL: require('./../images/facilities/conference.jpg')
  },
]