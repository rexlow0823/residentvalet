import { Actions } from 'react-native-router-flux';

import {
  REGISTER_VISITOR_SUCCESS,
  REGISTER_VISITOR_FAIL
} from './../actions/types';

const INITIAL_STATE = {
  status: '',
  title: '',
  message: ''
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case REGISTER_VISITOR_SUCCESS:
    case REGISTER_VISITOR_FAIL: 
      return { 
        ...state,
        status: action.payload.status,
        title: action.payload.title,
        message: action.payload.message
      }
    default:
      return state;
  }
}