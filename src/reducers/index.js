import { combineReducers } from 'redux';

import AuthReducer from './AuthReducer';
import ProfileReducer from './ProfileReducer';
import APIReducer from './APIReducer';

export default combineReducers({
  auth: AuthReducer,
  profile: ProfileReducer,
  api: APIReducer
})