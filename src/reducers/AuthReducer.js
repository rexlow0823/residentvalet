import { Actions } from 'react-native-router-flux';
import {
  LISTEN_TO_USER,
  FIREBASE_USER_EXISTED,
  FIREBASE_USER_NOT_EXISTED,
  NEXMO_VERIFY,
  NEXMO_CHECK,
  REGISTER_USER_SUCCESS,
  LOGIN_USER_SUCCESS,
  AUTH_FAIL,
  LOGOUT_USER
} from './../actions/types';

const INITIAL_STATE = { user: null, message: null };
const EXIST_STATE = { user: {}, message: null };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LISTEN_TO_USER:
      if (!action.payload) {
        return EXIST_STATE;
      }
      return {
        user: {
          email: action.payload.email,
          uid: action.payload.uid
        },
        error: null,
      };
    case FIREBASE_USER_EXISTED:
    case FIREBASE_USER_NOT_EXISTED:
    case NEXMO_VERIFY:
    case NEXMO_CHECK:
    case AUTH_FAIL:
      return { ...state, message: action.payload }
    case REGISTER_USER_SUCCESS:
    case LOGIN_USER_SUCCESS:
      Actions.main({ type: 'reset' });
      return EXIST_STATE;
    case LOGOUT_USER:
      return INITIAL_STATE;
    default:
      return state;
  }
}