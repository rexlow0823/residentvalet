import { Actions } from 'react-native-router-flux';
import { REHYDRATE } from 'redux-persist/constants';

import {
  GET_USER_PROFILE,
  STORE_IMAGE_LOCALLY,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAIL
} from './../actions/types';

const INITIAL_STATE = {
  name: '',
  email: '',
  phone: '',
  unit: '',
  residence: '',
  address: '',
  avatar: null
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case GET_USER_PROFILE:
      return { 
        ...state, 
        name: action.payload.name, 
        email: action.payload.email, 
        phone: action.payload.phone,
        unit: action.payload.unit,
        residence: action.payload.residence,
        address: action.payload.address
      }
    case STORE_IMAGE_LOCALLY:
      return { ...state, avatar: action.payload.uri }
    case REHYDRATE:
      var incoming = action.payload.profile;
      if(incoming) return { ...state, ...incoming }
      return state;
    default:
      return state;
  }
}