import React, { Component } from 'react';
import { View, StatusBar, TouchableOpacity } from 'react-native';

import { Scene, Router, Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as actions from './actions';

import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './styles';

import SplashScreen from './containers/SplashScreen';
import PreLogin from './containers/auth/PreLogin';
import Login from './containers/auth/Login';
import NexmoVerify from './containers/auth/NexmoVerify';

import Home from './containers/main/Home';
import EditProfile from './containers/main/EditProfile';
import Notification from './containers/main/Notification';
import RegisterVisitor from './containers/main/RegisterVisitor';
import CallAdmin from './containers/main/CallAdmin';
import BookFacilities from './containers/main/BookFacilities';
import AboutUs from './containers/main/AboutUs';

class RouterComponent extends Component {

  componentWillMount() {
    this.props.listenToUser();
  }

  renderBackButton = () => {
    return (
      <TouchableOpacity style={styles.navBarButton} onPress={() => Actions.pop()}>
        <Ionicons name="md-arrow-back" size={26} color="#323232" />
      </TouchableOpacity>
    )
  }

  renderCloseButton = () => {
    return (
      <TouchableOpacity style={styles.navBarButton} onPress={() => Actions.pop()}>
        <Ionicons name="md-close" size={26} color="#323232" />
      </TouchableOpacity>
    )
  }

  renderMailButton = () => {
    return (
      <TouchableOpacity style={styles.navBarButton} onPress={() => Actions.aboutUs()}>
        <Ionicons name="md-mail" size={22} color="#323232" />
      </TouchableOpacity>
    )
  }

  renderRightButton = () => {
    return (
      <TouchableOpacity style={styles.navBarButton} onPress={() => Actions.notification()}>
        <Ionicons name="ios-notifications" size={22} color="#323232" />
      </TouchableOpacity>
    )
  }

  renderCheckButton = () => {
    return (
      <TouchableOpacity style={styles.navBarButton} onPress={() => console.log('clear noti')}>
        <Ionicons name="md-done-all" size={22} color="#323232" />
      </TouchableOpacity>
    )
  }

  renderSaveButton = () => {
    return (
      <TouchableOpacity style={styles.navBarButton} onPress={() => console.log('clear noti')}>
        <Ionicons name="md-checkmark" size={22} color="#323232" />
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor="rgba(0,0,0,1)"
          translucent={false}
          barStyle="dark-content"
        />
        <Router
          titleStyle={styles.navTitleStyle}
          navigationBarStyle={styles.navigationBarStyle}>
          <Scene key="auth" hideNavBar>
            <Scene key="splash" component={SplashScreen} />
            <Scene key="preLogin" component={PreLogin} />
            <Scene key="login" component={Login} />
            <Scene key="nexmoVerify" component={NexmoVerify} />
          </Scene>
          <Scene key="main">
            <Scene key="home" component={Home} title="RESIDENT VALET" renderRightButton={this.renderRightButton} renderLeftButton={this.renderMailButton} />
            <Scene key="notification" component={Notification} title="NOTIFICATIONS" renderRightButton={this.renderCheckButton} renderBackButton={this.renderBackButton} />
            <Scene key="editProfile" component={EditProfile} title="PROFILE" renderBackButton={this.renderBackButton} />
            <Scene key="registerVisitor" component={RegisterVisitor} title="VISITOR" renderBackButton={this.renderBackButton} />
            <Scene key="callAdmin" component={CallAdmin} title="MANAGEMENT" schema="modal" direction="vertical" renderBackButton={this.renderCloseButton} />
            <Scene key="aboutUs" component={AboutUs} title="ABOUT US" hideNavBar direction="leftToRight" />
            <Scene key="bookFacilities" component={BookFacilities} title="OUR FACILITIES" schema="modal" direction="vertical" renderBackButton={this.renderCloseButton} />
          </Scene>
        </Router>
      </View>
    )
  }
}

export default connect(null, actions)(RouterComponent);